package com.somospnt.security.hierarchy.controller;

import java.security.Principal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HierarchyController {

    @GetMapping("/admin")
    public String admin(Principal principal) {
        return "Principal: "+principal.getName()+" Admin";
    }
    
    @GetMapping("/supervisor")
    public String supervisor(Principal principal) {
        return "Principal: "+principal.getName()+" Supervisor";
    }

    @GetMapping("/user")
    public String user(Principal principal) {
        return "Principal: "+principal.getName()+" User";
    }

}

package com.somospnt.security.hierarchy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoSpringSecurityHierarchyApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoSpringSecurityHierarchyApplication.class, args);
	}
}
